module es.cipfpbatoi.di.ajedrez {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;
    requires java.desktop;

    opens es.cipfpbatoi.di.ajedrez to javafx.fxml;
    exports es.cipfpbatoi.di.ajedrez;
}