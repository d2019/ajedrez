package es.cipfpbatoi.di.ajedrez;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application implements EventHandler<MouseEvent> {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        stage.setTitle("Ajedrez");

        int x = 0; int y = 0;

        Group root = new Group();
        Scene scene = new Scene(root, 400, 400);
        stage.setScene(scene);

        for (int i = 0; i < 8; i++) {
            x = 0;
            for (int j = 0; j < 8; j++) {

                Rectangle rectangle = new Rectangle(50, 50);

                if ((i + j) % 2 == 0) {
                    rectangle.setFill(Color.DEEPSKYBLUE);
                } else {
                     rectangle.setFill(Color.WHITE);
                }

                rectangle.setX(x);
                rectangle.setY(y);
                rectangle.setOnMouseClicked(this);
                Text text = new Text();
                text.setX(400);
                text.setY(200);

                root.getChildren().add(rectangle);
                root.getChildren().add(text);
                x += 50;
            }
            y += 50;
        }

        stage.show();
    }

    @Override
    public void handle (MouseEvent mouseEvent){
        System.out.println("Has clickado en la casilla ");
    }

    public static void main(String[] args) {
        launch();
    }
}